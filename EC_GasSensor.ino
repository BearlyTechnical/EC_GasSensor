#define ADC_VREF 3.3
#define TEMP_SENSOR_PIN A5
#define CO2_SENSOR_PIN A6
#define O2_SENSOR_PIN A7

//Change this to match with desired sampling frequency (in milliseconds)
const uint16_t sample_interval_ms = 128;

/*
 * This function does the sampling and printing of gas concentration values.
 * Assumes that Serial.begin() has been called in setup.
 */
void doAndPrintGasMeasurements(){

    long sumO2 = 0;
    long sumCO2 = 0;
    for(int i=0; i<32; i++) {
        sumO2 += analogRead(O2_SENSOR_PIN);
        sumCO2 += analogRead(CO2_SENSOR_PIN);
        delay(sample_interval_ms/32);
    }
    sumO2 >>= 5;
    sumCO2 >>= 5;

    float cnO2 = sumO2 * (ADC_VREF / 1023.0) * (0.21 / 2.0) * 100;
    float cnCO2 = 100 - sumCO2 * (ADC_VREF / 1023.0) * 100; //Naive, unscientific and uncalibrated CO2 reading roughly matching the scale of the O2

    Serial.print(cnO2);
    Serial.print(", ");
    Serial.print(cnCO2);
    Serial.print("\n");
    
}

void setup() {
    Serial.begin(115200);
    Serial.println("I: Board initialization OK");
    Serial.println("L: O2 concentration, CO2 relative");
}

void loop() {
  doAndPrintGasMeasurements();
}

