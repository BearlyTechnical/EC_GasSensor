# Arduino based O2 + CO2 sensor for EpicChallenge

Based on ME2-O2 oxygen gas sensor and MG811 carbon dioxide gas sensor.

![example_build](./example_build.png "Example build")

## Schematics

![schematic](./schematic.png "Schematic")

Connect GND and 5V power for both sensors and connect O2 sensor output to A7, and CO2 sensor output to A6 on the arduino. The temperature probe on the CO2 module can be connected to the A5 pin, but current software doesn't do temperature compensation.


## Hardware

| Amount   | Item                   | Notes                                                                                             |
|---------:|:-----------------------|---------------------------------------------------------------------------------------------------|
|    1 pcs | Arduino Nano           |                                                                                                   |
|    1 pcs | Arduino Nano breakout  |                                                                                                   |
|  enough  | Wires and connectors   |                                                                                                   |
|    1 pcs | Grove O2 Sensor module |                                                                                                   |
|    1 pcs | MG811 Sensor module    |                                                                                                   |
|  4-8 pcs | Self tapping screw     | If using the lasercut base, you'll need to replace these with eg. standoffs and M3 nuts and bolts.|

You can either 3D print the full base, or lasercut the base and print/get the standoffs and adapters.

| File                           | Purpose                                                                              |
|--------------------------------|--------------------------------------------------------------------------------------|
| ecgs_base_full_wfanholes.stl   | Use this if you don't want to mount the fan or use generic standoffs for fan mounting|
| ecgs_base_full_wfanpillars.stl | This model has integrated support pillars for the fan                                |
| ecgs_grove_ME2O2_mount.stl     | If you lasercut the base, you can print this piece to mount the Grove-sensor         |
| ecgs_baseplate_3mm_acrylic.svg | Vectors for lasercutting the base                                                    |
